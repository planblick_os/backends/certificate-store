#!/bin/sh
cd /src/app
python -m pytest -sv --junitxml=/src/test_results/test_report.xml --html=/src/test_results/test_report.html --self-contained-html /src/app/tests

