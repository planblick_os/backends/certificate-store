import json

from pbglobal.commands.createNewCertificate import createNewCertificate
from pbglobal.events.newCertificateCreated import newCertificateCreated
from time import sleep

import traceback

def event_handler(ch, method=None, properties=None, body=None):
    try:
        if method.routing_key == "createNewCertificate":
            payload = json.loads(body.decode())
            print("createNewCertificate event handling", payload)
            newCertificateCreatedEvent = newCertificateCreated(skip_validation=True).from_json(newCertificateCreated, payload)
            newCertificateCreatedEvent.publish()
            ch.basic_ack(method.delivery_tag)

        elif method.routing_key == "newCertificateCreated":
            payload = body.decode()
            payload_json = json.loads(payload)
            print("newCertificateCreated event handling", payload_json)

            ch.basic_ack(method.delivery_tag)

        else:
            print("NO HANDLER FOR routing key: ", method.routing_key)
            ch.basic_ack(method.delivery_tag)
    except Exception as e:
        print(e)
        traceback.print_exc()
        ch.basic_nack(method.delivery_tag)
        sleep(3)
