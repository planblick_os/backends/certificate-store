import pytest

from models.certificates import Certificates, CertificateNotFound


def test_get_legacy_id_rsa():
    for_service = "test"
    cert_content = Certificates().get_legacy_idrsa(for_service)
    assert type(cert_content) == str
    assert "BEGIN RSA PRIVATE KEY" in cert_content

def test_get_legacy_id_rsa_pem():
    for_service = "test"
    cert_content = Certificates().get_legacy_idrsa_pem(for_service)
    assert type(cert_content) == str
    assert "BEGIN RSA PUBLIC KEY" in cert_content

def test_get_legacy_id_rsa_pub():
    for_service = "test"
    cert_content = Certificates().get_legacy_idrsa_pub(for_service)
    assert type(cert_content) == str
    assert cert_content.startswith("ssh-rsa")

def test_get_legacy_id_rsa_non_existing():
    with pytest.raises(CertificateNotFound):
        for_service = "nonexisting"
        cert_content = Certificates().get_legacy_idrsa(for_service)


def test_get_legacy_id_rsa_pem_non_existing():
    with pytest.raises(CertificateNotFound):
        for_service = "nonexisting"
        cert_content = Certificates().get_legacy_idrsa_pem(for_service)


def test_get_legacy_id_rsa_pub_non_existing():
    with pytest.raises(CertificateNotFound):
        for_service = "nonexisting"
        cert_content = Certificates().get_legacy_idrsa_pub(for_service)

def test_get_legacy_certificates_as_dict():
    for_service = "test"
    return_value = Certificates().get_legacy_certificates_for(service_name=for_service)

    assert type(return_value.get("id_rsa")) == str
    assert len(return_value.get("id_rsa")) > 0
    assert "BEGIN RSA PRIVATE KEY" in return_value.get("id_rsa")

    assert type(return_value.get("id_rsa.pem")) == str
    assert len(return_value.get("id_rsa.pem")) > 0
    assert "BEGIN RSA PUBLIC KEY" in return_value.get("id_rsa.pem")

    assert type(return_value.get("id_rsa.pub")) == str
    assert len(return_value.get("id_rsa.pub")) > 0
    assert "ssh-rsa" in return_value.get("id_rsa.pub")
